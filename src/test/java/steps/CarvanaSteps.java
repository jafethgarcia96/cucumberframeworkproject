package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import pages.*;
import utilities.Driver;
import utilities.Waiter;

public class CarvanaSteps{
    public WebDriver driver;
    public CarvanaHomePage carvanaHomePage;
    public CarvanaCarFinderSearchPage carvanaCarFinderSearchPage;
    public CarvanaTryCarFinderPage carvanaTryCarFinderPage;
    public CarvanaSellMyCarPage carvanaSellMyCarPage;
    public CarvanaGetYourOfferPage carvanaGetYourOfferPage;
    public Actions actions;
    CarvanaAutoLoanCalcLoanPage carvanaAutoLoanCalcLoanPage;

    @Before
    public void setUp(){
        driver = Driver.getDriver();
        carvanaHomePage = new CarvanaHomePage(driver);
        carvanaCarFinderSearchPage = new CarvanaCarFinderSearchPage(driver);
        carvanaTryCarFinderPage = new CarvanaTryCarFinderPage(driver);
        carvanaSellMyCarPage = new CarvanaSellMyCarPage(driver);
        carvanaGetYourOfferPage = new CarvanaGetYourOfferPage(driver);
        actions = new Actions(driver);
        carvanaAutoLoanCalcLoanPage = new CarvanaAutoLoanCalcLoanPage(driver);
        }

    @Given("user is on {string}")
    public void userIsOn(String url) {
        driver.get(url);
    }

    @When("user clicks on {string} menu item")
    public void userClicksOnMenuItem(String carFinderMenuItem) {
        switch (carFinderMenuItem){
            case CarvanaTexts.carFinder:
                Waiter.waitForWebElementToBeClickable(driver,6,carvanaHomePage.carFinderMenuItem);
                carvanaHomePage.carFinderMenuItem.click();
                break;
            case "SELL/TRADE":
                //Waiter.waitForWebElementToBeClickable(driver, 10, carvanaHomePage.sellTradeMenuItem);
                driver.navigate().refresh();
                carvanaHomePage.sellTradeMenuItem.click();
                break;
            case "AUTO LOAN CALCULATOR" :
                carvanaHomePage.loanCalculator.click();
                break;
            default:
                System.out.println("No Such Button or Not Matching");

        }
    }

    @Then("user should be navigated to {string}")
    public void userShouldBeNavigatedTo(String url) {
        Assert.assertEquals(url, driver.getCurrentUrl());
    }

    @Then("user should see {string} text")
    public void userShouldSeeText(String string) {
        switch (string){
            case CarvanaTexts.whatCarShouldIGetTxt:
                Assert.assertTrue(carvanaCarFinderSearchPage.whatCarShouldIGetHeader.isDisplayed());
                Assert.assertEquals(CarvanaTexts.whatCarShouldIGetTxt,carvanaCarFinderSearchPage.whatCarShouldIGetHeader.getText() );
                break;
            case CarvanaTexts.carFinderCanHelpTxt:
                Assert.assertTrue(carvanaCarFinderSearchPage.headerMessage.isDisplayed());
                Assert.assertEquals(CarvanaTexts.carFinderCanHelpTxt,carvanaCarFinderSearchPage.headerMessage.getText() );
                break;
            case CarvanaTexts.whatsMostImportantToYouTxt:
                Assert.assertTrue(carvanaTryCarFinderPage.headerImportantToYouMessage.isDisplayed());
                Assert.assertEquals(CarvanaTexts.whatsMostImportantToYouTxt ,carvanaTryCarFinderPage.headerImportantToYouMessage.getText());
                break;
            case CarvanaTexts.selectUpTo4txt:
                Assert.assertTrue(carvanaTryCarFinderPage.headerSelectUpTo4.isDisplayed());
                Assert.assertEquals( CarvanaTexts.selectUpTo4txt, carvanaTryCarFinderPage.headerSelectUpTo4.getText());
                break;
            case CarvanaTexts.getARealOffer:
                Assert.assertTrue(carvanaSellMyCarPage.headerGetARealOfferIn2Min.isDisplayed());
                Assert.assertEquals(CarvanaTexts.getARealOffer,carvanaSellMyCarPage.headerGetARealOfferIn2Min.getText() );
                break;
            case CarvanaTexts.wePickUpYourCar:
                Assert.assertTrue(carvanaSellMyCarPage.wePickUpYourCarHeader.isDisplayed());
                Assert.assertEquals( CarvanaTexts.wePickUpYourCar,carvanaSellMyCarPage.wePickUpYourCarHeader.getText());
                break;
            case CarvanaTexts.cantFindVinMessage:
                Waiter.waitForWebElementToBeVisible(driver,60,carvanaGetYourOfferPage.cantFindVinMessage);
                Assert.assertTrue(carvanaGetYourOfferPage.cantFindVinMessage.isDisplayed());
                Assert.assertEquals(CarvanaTexts.cantFindVinMessage,carvanaGetYourOfferPage.cantFindVinMessage.getText() );
                break;
            default:
                System.out.println(string + " Not Found");

        }

    }

    @Then("user should see {string} link")
    public void userShouldSeeLink(String message) {
        switch (message){
            case "TRY CAR FINDER":
                Assert.assertTrue(carvanaCarFinderSearchPage.tryCarFinderLink.isDisplayed());
                break;
            default:
                System.out.println(message + "Not Found");
        }
    }

    @When("user clicks on {string} link")
    public void userclicksonlink(String button) {
        switch (button){
            case "TRY CAR FINDER":
                carvanaCarFinderSearchPage.tryCarFinderLink.click();
                break;
            default:
                System.out.println(button + "Not Found");
        }
    }

    @When("user clicks on {string} button")
    public void userClicksOnButton(String button) {

        switch (button){
            case "VIN":
                carvanaSellMyCarPage.vinButton.click();
                break;
            case "GET MY OFFER":
                carvanaSellMyCarPage.getMyOfferButton.click();
                break;
            default:
                System.out.println(button + " Not Found");
        }
    }

    @When("user enters vin number as {string}")
    public void userEntersVinNumberAs(String vin) {
        carvanaSellMyCarPage.inputVinButton.sendKeys(vin);
    }

    @When("user hovers over on {string} menu item")
    public void userHoversOverOnMenuItem(String string) {
        //driver.navigate().refresh();
        Waiter.pause(3);
        actions.moveToElement(carvanaHomePage.financingMenuItem).perform();
    }

    @When("user enters {string} as {string}")
    public void userEntersAs(String string, String string2) {
        switch (string){
            case "Cost of Car I want":
                carvanaAutoLoanCalcLoanPage.costOfCarIWantInput.sendKeys(string2);
                break;
            case "What is Your Down Payment?":
                carvanaAutoLoanCalcLoanPage.downPaymentInput.sendKeys(string2);
                break;
            default:
                System.out.println(string + " not found!");

        }

    }

    @When("user selects {string} as {string}")
    public void userSelectsAs(String string, String string2) {
        // Write code here that turns the phrase above into concrete actions
        Select select;
        switch (string){
            case "What’s Your credit Score?":
                select = new Select(carvanaAutoLoanCalcLoanPage.selectCreditScore);
                select.selectByVisibleText(string2);
                break;
            case "Choose Your Loan Terms":
                select = new Select(carvanaAutoLoanCalcLoanPage.loanTerm);
                select.selectByVisibleText(string2);
                break;
            default:
                System.out.println(string + " not found!");
        }


    }

    @Then("user should see the monthly payment as {string}")
    public void userShouldSeeTheMonthlyPaymentAs(String string) {
        Assert.assertTrue(carvanaAutoLoanCalcLoanPage.monthlyPaymentsValue.isDisplayed());
        Assert.assertEquals(string, carvanaAutoLoanCalcLoanPage.monthlyPaymentsValue.getText());
    }

}
