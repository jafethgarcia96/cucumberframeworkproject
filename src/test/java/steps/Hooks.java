package steps;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import org.junit.After;
import utilities.Driver;

public class Hooks {

    @Before
    public void testSetUp(Scenario scenario){
        System.out.println(scenario.getName() + " is started!");
    }

    @After
    public void tearDown(){
        Driver.quitDriver();
    }
}
