package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarvanaSellMyCarPage {

    public CarvanaSellMyCarPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[contains(@class,'fKlQzD')]")
    public WebElement headerGetARealOfferIn2Min;

    @FindBy(xpath = "//div[contains(@class,'Text-sc-1m69wc4')]")
    public WebElement wePickUpYourCarHeader;

    @FindBy(xpath = "//button[@data-cv-test='VINToggle']")
    public WebElement vinButton;

    @FindBy(xpath = "//input[@class='FormInput-oo6j68-5 jLejJf']")
    public WebElement inputVinButton;

    @FindBy(xpath = "//button[@data-cv-test='heroGetMyOfferButton']")
    public WebElement getMyOfferButton;



}
