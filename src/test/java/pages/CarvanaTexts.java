package pages;

public class CarvanaTexts {
    //home page
    public static final String carFinder = "CAR FINDER";

    //car finder page
    public static final String whatCarShouldIGetTxt = "WHAT CAR SHOULD I GET?";
    public static final String carFinderCanHelpTxt = "Car Finder can help! Answer a few quick questions to find the right car for you.";

    //try car finder page
    public static final String whatsMostImportantToYouTxt = "What is most important to you in your next car?";
    public static final String selectUpTo4txt = "Select up to 4 in order of importance";

    //sell my car page
    public static final String getARealOffer = "GET A REAL OFFER IN 2 MINUTES";
    public static final String wePickUpYourCar = "We pick up your car. You get paid on the spot.";
    public static final String cantFindVinMessage = "We couldn’t find that VIN. Please check your entry and try again.";


}
