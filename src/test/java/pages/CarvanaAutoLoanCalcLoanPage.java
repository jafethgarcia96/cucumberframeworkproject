package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarvanaAutoLoanCalcLoanPage {
    public CarvanaAutoLoanCalcLoanPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "form-control")
    public WebElement costOfCarIWantInput;

    @FindBy(id = "creditBlock")
    public WebElement selectCreditScore;

    @FindBy(xpath = "//select[@name='loanTerm']")
    public WebElement loanTerm;

    @FindBy(xpath = "//input[@name='downPayment']")
    public WebElement downPaymentInput;

    @FindBy(className = "loan-calculator-display-value")
    public WebElement monthlyPaymentsValue;


}
