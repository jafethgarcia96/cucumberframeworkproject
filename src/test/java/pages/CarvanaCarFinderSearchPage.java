package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarvanaCarFinderSearchPage {
    public CarvanaCarFinderSearchPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(tagName = "h1")
    public WebElement whatCarShouldIGetHeader;

    @FindBy(tagName = "h3")
    public WebElement headerMessage;

    @FindBy(xpath = "//a[@data-qa='router-link']")
    public WebElement tryCarFinderLink;


}
