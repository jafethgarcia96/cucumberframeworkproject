package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarvanaHomePage {
    public CarvanaHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[@data-cv-test='headerCarFinderLink']")
    public WebElement carFinderMenuItem;

    @FindBy(xpath = "//div[@data-qa='header-items']/a[2]")
    public WebElement sellTradeMenuItem;

    @FindBy(xpath = "//div[@data-cv-test='headerFinanceDropdown']/a")
    public WebElement financingMenuItem;

    @FindBy(xpath = "//a[@data-cv-test='headerFinanceLoanCalc']")
    public WebElement loanCalculator;

}
