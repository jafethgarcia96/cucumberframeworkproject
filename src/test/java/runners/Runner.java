package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",//starts from project lvl
        glue = "steps", //dflt starts from java pckg
        tags = "@Test2" ,//will run all b/c empty
        dryRun = false,
        plugin = "html:target/cucumber-reports"

)
public class Runner {

}
